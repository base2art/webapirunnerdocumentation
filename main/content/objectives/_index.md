+++
title = "Why?"
description = ""
date = "2017-04-24T18:36:24+02:00"

+++



Objective
----
Provide a simple and consistent framework for building ReST web services.

Note: that I am porting this to aspnetcore from aspnet.MVC so this documentation is subject to change in the next version, but backward compatibility is a high priority.

Goals
----

* Writing a web service is no more complex than writing a simple class.
* All applications have easy access to call scheduled tasks in their API.
* All ReST services are automatically documented with any boilerplate code or configuration.
* All ReST services are platform agnostic ( easily portable to another platform )
* All ReST services are implemented with a standard set of boiler plate that makes them easy to build.
* Building packages for deployments is trivia.
* Supports multiple deployment scenarios (IIS or Console)
* All ReST services must be fully unit testable
* All ReST services be fully integration testable

Strategy
----
Base2art's WebApiRunner is a collection of dlls, and exes that in different deployment scenarios allows developers to load their custom built dlls and assign routes to class in the dlls, which will automatically have webservices create around them. Object bindings and routes are managed via yaml configuration.

All applications are made of three major ReST request types:

#### Endpoints
TODO

#### Tasks
TODO

#### Healthchecks
TODO

Major Components
----

* A YAML configuration system that allows for environment based configuration options
* Built-in IOC (dependency Resolver) managed entirely through configuration
* Prebuilt Filters
* CoRS support that you can easily understand
* Prebuilt Exception Loggers and handlers
* Prebuilt input/output formatters
* Middleware support layer for extension
* An object Cache
* Configuration based media-type mapping  
* ReST wrapped aspects
* Simple Route binding
* Api Exploration that works well
* A simple bundler for deployment
* A json Serializer (will be removed in the dotnet-core port)
* A optional JWT serializer/deserializer (will be isolated to its own dll in the dotnet-core port)

#### Dependencies
* Swagger
* Newtownsoft (removing in next version)
* yamldotnet