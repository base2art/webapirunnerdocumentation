+++
title = "Simple Project Walk through"
description = ""
date = "2017-04-24T18:36:24+02:00"

+++


##### From scratch (no pre-existing project)

Find the code to this tutorial here:

https://bitbucket.org/base2art/nearlyblankwebapirunnerproject

##### Create a new Class Library in your IDE of choice (VS / Rider / SharpDevelop)

I usually put my class library inside a `src` folder to make easily identify source code from configuration and test code. This is not necessary but I will assume this throughout.

##### Create 4 classes

All namespaces and class names are not necessary, but convention to make it easier to understand.

1. A task class (named: Samples.Repositories.CountryService)
    * A class that writes to the datastore new and existing values, in this case the data store isthe filesystem.
2. An endpoint class (named: Samples.Public.Endpoints.CountryService)
    * This class is responsible for taking in new countries, deleting countries and returning a listof counries with user interaction. It will accomplish this by reading from the database(Repository class) and caching the result for subsequent calls.
3. A Healthcheck class (named: Samples.Public.Endpoints.CountryServiceHealthCheck)
    * This is a class that will tell you the health of the application specifically, are coutriesbeing returned from the database ok.
4. A task class (named: Samples.Public.Endpoints.ClearCountryCacheTask)
    * This is a class will be ultimately triggered on a timer to clear the cache periodically so thatold values are automatically expired.


##### Let's add some simple methods to the Repository class:

If you have not already referenced `Newtonsoft.Json` via nuget you will need to do that now.

* GetCountriesFromDatabase()
* AddCountryToDatabase(int id, string name)
* DeleteCountryFromDatabase(int id)
{{< expand-code >}}
namespace Samples.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    
    public class FileSystemCountryRepository
    {
        private readonly string filename;

        public FileSystemCountryRepository(string filename)
        {
            this.filename = filename;
        }

        public Task DeleteCountryFromDatabase(int id)
        {
            return this.AugmentCollection(x =>
                {
                    if (x.ContainsKey(id))
                        x.Remove(id);
                });
            
        }

        public Task AddCountryToDatabase(int id, string name)
        {
            return this.AugmentCollection(x => x[id] = name);
        }

        // in another scenario you may fetch from a database.
        // this is just a simple example.
        public Task<List<KeyValuePair<int, string>>> GetCountriesFromDatabase()
        {
            return Task.Factory.StartNew(() =>
                {
                    if (!File.Exists(this.filename))
                    {
                        return new List<KeyValuePair<int, string>>();
                    }
                    
                    var savedItems = JsonConvert.DeserializeObject<Dictionary<int, string>>(File.ReadAllText(this.filename));
                    return savedItems.ToList();
                                             
                });
        }
        
        private Task AugmentCollection(Action<Dictionary<int, string>> processor)
        {
            if (processor == null)
            {
                return Task.FromResult(true);
            }
            
            return Task.Factory.StartNew(() =>
                {
                    Dictionary<int, string> localKnownCountries = new Dictionary<int, string>();
                    if (File.Exists(this.filename))
                    {
                        var content = File.ReadAllText(filename);
                        localKnownCountries = JsonConvert.DeserializeObject<Dictionary<int, string>>(content);
                    }
                                             
                    processor(localKnownCountries);
                                             
                    var outputContent = JsonConvert.SerializeObject(localKnownCountries);
                    File.WriteAllText(this.filename, outputContent);
                });
        }
    }
}

{{</ expand-code >}}

##### Let's add some simple methods to the Endpoint class:

This Endpoint class will have caching and some minor code to that helps perform a health check later 
{{< expand-code >}}
namespace Samples.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Samples.Repositories;

    public class CountryService
    {
        private readonly FileSystemCountryRepository countries = new FileSystemCountryRepository("countries.cache");
        
        private static Dictionary<int, string> knownCountries = null;
        
        public async Task<List<KeyValuePair<int, string>>> GetCounties()
        {
            var localKnownCountries = knownCountries;
            if (localKnownCountries == null)
            {
                Func<Task<List<KeyValuePair<int, string>>>> lookupData = async () =>{
                    try
                    {
                        var result = await this.countries.GetCountriesFromDatabase();
                        Healthchecks.CountryServiceHealthCheck.Clear();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        Healthchecks.CountryServiceHealthCheck.AddException(ex);
                        throw;
                    }
                };
                
                
                // avoid null refs and locking...
                // but you might fetch the data 2 times...
                localKnownCountries = (await lookupData()).ToDictionary(x => x.Key, x => x.Value);
                knownCountries = localKnownCountries;
            }
            
            return localKnownCountries.ToList();
        }
        
        public async Task AddCountry(int id, string name)
        {
            await this.countries.AddCountryToDatabase(id, name);
            ClearCachedData();
        }

        public async Task DeleteCountry(int id)
        {
            await this.countries.DeleteCountryFromDatabase(id);
            ClearCachedData();
        }

        public static void ClearCachedData()
        {
            knownCountries = null;
        }
    }
}
{{</ expand-code >}}


##### Let's add some code to our healthcheck class:

The primary method is `Task ExecuteAsync();` throw an exception to see it to fail.

{{< expand-code >}}
namespace Samples.Public.Healthchecks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    
    public class CountryServiceHealthCheck
    {
        private static readonly List<Exception> lastExceptions = new List<Exception>();
        
        public Task ExecuteAsync()
        {
            var items = lastExceptions.Skip(0).Take(4).ToArray();
            if (items.Length > 4)
            {
                throw new AggregateException(items);
            }
            
            return Task.FromResult(true);
        }
        
        public static void AddException(Exception ex)
        {
            lastExceptions.Insert(0, ex);
        }

        public static void Clear()
        {
            lastExceptions.Clear();
        }
    }
}
{{</ expand-code >}}

##### Let's add some code to our tasks class:

The primary method is `Task ExecuteAsync();`

{{< expand-code >}}
namespace Samples.Public.Tasks
{
    using System.Threading.Tasks;
    using Samples.Public.Endpoints;
    
    public class ClearCountryCacheTask
    {
        public Task ExecuteAsync()
        {
            CountryService.ClearCachedData();
            return Task.FromResult(true);
        }
    }
}
{{</ expand-code >}}

##### Lets add the web api runner to our code

Notice at this point we have created a plain class library (dll) that has not WebService dependencies.

Choose your project and Add a nuget Dependency of `Base2art.WebApiRunner.Deployer` to it. With the powershell console the command is `Install-Package Base2art.WebApiRunner.Deployer -Version 0.0.1.10`. This will not add any dependencies to the project it will merely add the runner tools to the `packages` directory.

##### Enable debugging

Set the `Start Action` to "Program" or "External Program" depending on what you IDE says, with start path of `$(SolutionDir)\packages\Base2art.WebApiRunner.UnversionedDebugger.exe`.
The command line arguments should be `server-with-verify "$(SolutionDir)\conf\configuration-local-debug-server.yaml"`
And the working dir is `.\`

If you are editing the csproj directly it should look like the following.
```
    <StartAction>Program</StartAction>
    <StartProgram>$(SolutionDir)\packages\Base2art.WebApiRunner.UnversionedDebugger.exe</StartProgram>
    <StartArguments>server-with-verify "$(SolutionDir)\conf\configuration-local-debug-server.yaml"</StartArguments>
    <StartWorkingDirectory>.\</StartWorkingDirectory>
```

##### Wire-up Endpoints, healthchecks, and Tasks

Create a new yaml file `"$(SolutionDir)\conf\configuration-local-debug-server.yaml"` with the following content:


```

## Configure ports for the server for the console runner
serverSettings:
  http:
  - host: localhost
    rootPath: /
    port: 8080

adminServerSettings:
  http:
  - host: localhost
    rootPath: /
    port: 8081

## Configure the directory to look for assemblies
assemblySearchPaths:
  - "../src/NearlyBlankWebApiRunnerProject/bin/debug"

## Configure ReST endpoints
endpoints:
  urls:
    - url: "api/countries"
      type: Samples.Public.Endpoints.CountryService
      verb: GET
      method: GetCountries
      
    - url: "api/countries"
      type: Samples.Public.Endpoints.CountryService
      verb: POST
      method: AddCountry
      
    - url: "api/countries"
      type: Samples.Public.Endpoints.CountryService
      verb: DELETE
      method: DeleteCountry
    

## Configure tasks
tasks:
  items:
    - name: clear-caches
      type: Samples.Public.Tasks.ClearCountryCacheTask
      interval: 01:00:00

## Configure healthchecks      
healthchecks:
  items:
    - name: clear-caches
      type: Samples.Public.Healthchecks.CountryServiceHealthCheck

```



#### You're ready!

Notice that at no point we added references to dlls outside of our class library and newtonsoft required to write content to disk.

You can now start debugging (F5 or start button).

View your endpoints at:

[ Endpoints at http://localhost:8080/swagger/ui/index ] (http://localhost:8080/swagger/ui/index)
[ Tasks and Healthchecks at http://localhost:8081/swagger/ui/index ] (http://localhost:8081/swagger/ui/index)

